Drone_ToolChain_support
========================

This tool chain support elements were created as support scripts for connecting
the gaps between [Mission planner](http://ardupilot.org/planner/) and [Open drone Maps](https://www.opendronemap.org/) (ODM).
These scripts will help you with post processing your logs and images captured
from your UAV after a flight. They are python based program which will help you
gather logs from your mission planner logs folder and organise them by date,
geaotag images from csv files and modify them so as to work well Open Drone Maps.

>I will try my level best to explain how to get each script up and running, so
that you could use them out of the box.

>**Note**: All scripts in this folder has to be run from the required source
folder (other than index.py)

>I have also created a shell script which would help to execute these python scripts
as terminal commands. Please go thought the document to understand the working of each
scripts and get instructions on how to run each commands.   

Here we will have three sections.
### - [How to organise log files](https://gitlab.com/icfoss/drone/tool_chain_elements/tree/master#how-to-organise-log-files)
### - [How to geo-tag images so as to load them directly into ODM](https://gitlab.com/icfoss/drone/tool_chain_elements/tree/master#how-to-geo-tag-images)
### - [How to create backups for the flightlogs and the images](https://gitlab.com/icfoss/drone/tool_chain_elements/tree/master#how-to-take-backups)
### - [How To index images that will go in for processing](https://gitlab.com/icfoss/drone/tool_chain_elements/tree/master#how-to-index-images)
### - [How to add these python scripts as terminal commands](https://gitlab.com/icfoss/drone/tool_chain_elements/tree/master#how-to-auto-add-python-scripts-as-terminal-commands)
 -  [Usage](https://gitlab.com/icfoss/drone/tool_chain_elements/tree/master#usage)

>**If you dont wish to read through the working of each scripts and wish to just 
click-install these scripts and get on with your work jump to the 
[automate scripts](https://gitlab.com/icfoss/drone/tool_chain_elements/tree/master#automatesh) part**. 

## How to organise log files
At ICFOSS we follow the following folder structure to store our flight logs:
- Flight_Logs
  - EquipmentName 1
    - date1
    - date2
    - ....
  - EquipmentName 2
    - date1
    - date2
    - ....
  - EquipmentName 3
    - date1
    - date2
    - ....  

Here the logit.py script will help you create subfolders under the EquipmentName
folders according to dates in which log files were found in MissionPLanner/logs.
So, to initialise first create a folder with its name that of your equipment and
then link that Equipment folder using logit.py. The rest is automatic.

### logit.py

>*(requires [DateTime Package](https://pypi.org/project/DateTime/),
 [OS Module](https://www.pythonforbeginners.com/os/pythons-os-module),
 [shutil](https://docs.python.org/3.5/library/shutil.html),
 [platform](https://docs.python.org/3.5/library/platform.html),
 [subprocess](https://docs.python.org/3.5/library/subprocess)
 and runs in python3 )*

This script is intended to help organise flightlogs. Here the user should
download logs from the drones using Mission Planner. The program run on the
current working directory from which it is invocked. When ran initially
the program will ask to connect to the folder to which the Missionplanner
saves the logs *(when mission planne invocked as root, this folder is located at
/root/Mission\ Planner/logs/'type of drone'/, if ran as user it will be
located in you home folder ~/Mission\ Planner/logs/'type of drone'/)*. After then,
the program itself is self explanatory. Now the script will copy logs into
corresponding folding into the working directory it was initialised. From then,
to copy newly updated log files, just run logit.py

## How to geo-tag images
To geotag images you also need to install exiftool. We need to follow the
following sequence to get the images geotagged.
```
python3 modifyCSV.py
python tagit.py
exiftool -csv="modifiedlog.csv" "."
rm *.jpg_original
```
### tagit.py
>*(requires [pexif library](https://pypi.org/project/pexif/),
 and runs in python2 only, due to some errors in the pexif module for python3)*

After logging flight logs, use MissionPlanner to extract camera trigger
locations from the .bin files. Inorder to extract the location you need to run
the geotag function in Missionplanner *([click here](http://ardupilot.org/copter/docs/common-geotagging-images-with-mission-planner.html)
to learn how to geotag images with missionplanner)*. After running the geotag
function, there will be many files generated in the directory which contains the
images. From those we will require the location.txt and location.csv files for
geotagging the images.

### modifyCSV.py
>*(requires [OS Module](https://www.pythonforbeginners.com/os/pythons-os-module)
 and runs in python3)*

Used to modify location.csv so that exiftool can use the new created
modifiedlog.csv as parameter.

## How to take backups
Its always a good habbit to backup your flight logs and images acptured  into a
removable media. And for this, we need to backup only those newly added data,
and doing that manually is time consuming and tiring.
This backup.py script will help you backup those newly added files into your
removable media automatically ignoring those that are listed in the .backupignore.txt

### backup.py
>*(requires [OS Module](https://www.pythonforbeginners.com/os/pythons-os-module),
 [shutil](https://docs.python.org/3.5/library/shutil.html),
 [sys](https://docs.python.org/3.5/library/sys.html),
 [stat](https://docs.python.org/3.5/library/stat) and requires python3)*


This script will help backup the current working directory from which it is
invocked to the destination folder to which the parameter is linked.
Command Sytax:
```
python3 /<location in which script is located>/backup.py  /<destination
 location>/
```
## How To index images
This script is intended to help you with indexing images that are capture and
geotagged to those flight details that you wish to attach. This script store data
in a json files that will be located in the home folder.

The jason file will contain a list of dictionary of which each dict will contain:
```
    {
             "ID": ID,
             "area": Area,
            "equipment": equipment,
             "description": description,
             "date": date,
             "geotagged": geotagged,
             "camAngle" : angle,
             "flightType": flightType,
             "altitude" : altitude
             "images": [img1,img2,img3]
             "directory": location of images
     }
```
Image are not index by just their names but also by the date and time taken.
Which would help you index images taken in the same camera but in different
memory cards.
the usae of the program require paramaters:
  -  -s
    - Will help you search details of images in the target directory and display
    their index details
    ```
    python3 /<location in which script is located>/index.py -s  /<destination
     location>/
    ```
  -  -r
    - Will help you index new image in the destination register to those details
    that you enter
    ```
    python3 /<location in which script is located>/index.py -r  /<destination
     location>/
    ```
  -  -e
    - will help you edi the indexed details
    ```
    python3 /<location in which script is located>/index.py -e
    ```


## How to auto add python scripts as terminal Commands
To use these python scripts as terminal commands we need to [add our shell scripts](https://gitlab.com/icfoss/drone/tool_chain_elements/tree/master#automatesh)
as source to our bashrc so that you could initiate them as regular terminal commands.

## automate.sh
Run this shell script to auto install all the required python libraries and link 
all the above said pythons scripts so that you can run them as terminal commands.
>while installing the required libraries make sure that you learn how to use the 
[commands found here](https://gitlab.com/icfoss/drone/tool_chain_elements/tree/master#usage)

This shell script is used to auto create .droneMappingRunCommands.sh file into the
present working directory and add the file as a source into the .bashrc . 

**To run the shell script use the below command.**
```
chmod +x automate.sh
./automate.sh
```

After running your automate.sh shell script, close all your teminal windows and 
reopen it to load the our attached shell script. Now that your shell script is 
linked with your terminal, you call each commands in the usage pattern described
below..

### **Usage**:
 **1. Command to intitate organise logs from missionplanner in the present working directory**
```
logit
```

 **2. To initiate a [sequence of commands](https://gitlab.com/icfoss/drone/tool_chain_elements/tree/master#how-to-geo-tag-images) to tag images in the present working directory from the txt and csv files created by the Mission Planner.**
```
tagit
```

 **3. Used to backup or update files in the present working directory with that of the destination folder.**
```
backup /destination/folder/
```
>Note: any files deleted in the source directory will not be deleted from the destination folder in the next update.

 **4. Used to find the untagged images in the present working directory.**
```
finduntagged
```

 **5. To rename all the images with extensions as JPG to jpg.**
```
renamejpg
```



