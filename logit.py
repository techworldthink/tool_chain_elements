#     A flightlog logging script part of tool_chain_elements for GIS application
#     Copyright (C) 2018  ICFOSS
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#     Author: Vaisakh Anand (gitlab: @vaisakh032)
# /*******************************************************************************
#  * logit.py
#  *******************
#   This program will help you to organise your flight logs from Mission planner
#   So that you could easily go through your flight logs and manage them easily
#  *********************************************************************************


import datetime
import os
from shutil import copyfile
import platform
import subprocess

destinationFolder = os.getcwd()
MissionplannerLogs = []
loggerfile = ".logs.txt"
configfile = ".logitconfig.txt"


def colour(k):
    if k == "white":
        return "\033[0;37;40m"
    elif k == "grey":
        return "\033[1;30;40m"
    elif k == "yellow":
        return "\033[1;33;40m"
    elif k == "red":
        return "\033[1;31;40m"
    elif k == "loading":
        return "\033[0;31;42m"

def open_file(path):
    if platform.system() == "Windows":
        os.startfile(path)
    else:
        subprocess.Popen(["xdg-open", path])

def copylogs(dates,filename):
    destination = destinationFolder+'/'+dates+'/'+filename
    source = MissionplannerLogLocation+filename
    if os.path.exists(destination):
        if input(colour('red')+"File alread Exists, should I overwrite (y/N)?"+colour('white')) =='n':
            print(colour('yellow')+"Skipping File"+colour('white'))
            return
        else:
            copy = True
    else:
        copy = True
    if copy:
        print("copying file: ", filename, end=' ')
        copyfile(source, destination)
        print(colour('loading'),"\t ...100%",colour('white'))

def lastlogged():
    temp = open(configfile,'r')
    data = temp.readlines()
    date = datetime.datetime.now().strftime("%Y-%m-%d")
    temp.close()
    try:
        data[1] = "Last Logged:"+str(date)
    except:
        data.append("Last Logged:"+str(date))
    temp = open(configfile,'w')
    for ele in data:
        temp.write(str(ele.rstrip())+'\n')
    temp.close()


def backup(requiredlogs):
    count = 0
    haveboth = []
    haveone = []
    finalist = []
    for dates in requiredlogs:
        binfile = 0
        tlogfile = 0
        for log in MissionplannerLogs:
            if log.startswith(dates) and log.endswith('.tlog'):
                tlogfile = tlogfile+1
                continue
            if log.startswith(dates) and log.endswith('.bin'):
                binfile = binfile+1
                continue
        if tlogfile > 0 and binfile > 0:
            haveboth.append(dates)
            haveone.append(dates)
        else:
            haveone.append(dates)
        print(dates,"- Tlog : ",tlogfile," Bin in file: ",binfile)
    copyall = False
    if len(haveboth) > 0:
        print("Dates for which we have both Tlog and Bin file:",haveboth)
        if input("Should i Copy logs that have both tlog and bin files(y/n)?")=='y':
            print("Copying files!")
            finalist = haveboth
        else:
            copyall = True
    else:
        if input("No logs were found with both Tlog and Bin files should i copy them (y/n)?") == 'y':
            copyall = True
        else:
            exit()

    if copyall:
        finalist = haveone

    infofile = open(loggerfile,'a')
    for dates in finalist:
        print("Date:",dates,"\t Creating Destination Folder....",end=' ')
        if os.path.isdir(destinationFolder+'/'+dates):
            print("Directory Already Exists!")
        else:
            print("....complete")
            os.mkdir(destinationFolder+'/'+dates)
        for filename in MissionplannerLogs:
            if (filename.split(" ", 1)[0]) == dates:
                if filename.endswith('.bin') or filename.endswith('.tlog'):
                    copylogs(dates,filename)
        infofile.write(str(dates)+('\n'))
        print("***")
    infofile.close()
    lastlogged()


def initialise():
    temp = open(loggerfile,"w+")
    temp.close()
    temp = open(configfile,"w+")
    while True:
        missionplannnerlogLocation = input("Please input Missionplanner Log Location:")
        if os.path.isdir(missionplannnerlogLocation):
            temp.write("MissionPLannerLog="+missionplannnerlogLocation)
            break
        else:
            print("Please enter a valid File location")
    temp.close()

def CurrentStatus():
    global MissionplannerLogLocation
    global MissionplannerLogs
    if not os.path.exists(destinationFolder+'/'+loggerfile):
        print("Welcome to logit")
        initialise()
    MissionplannerLogLocation  = open(configfile).readlines()[0].split('=')[1].rstrip()
    MissionplannerLogs = os.listdir(MissionplannerLogLocation)
    infofile = open(loggerfile,'r')
    loglist= infofile.read().splitlines()
    infofile.close()
    #print(loglist)
    dates = [datetime.datetime.strptime(timestamps, "%Y-%m-%d") for timestamps in loglist]
    dates.sort()
    LOGsorteddates = [datetime.datetime.strftime(timestamps, "%Y-%m-%d") for timestamps in dates]
    try:
        print("Last log backup on:",open(configfile).readlines()[1].split(':')[1])
    except:
        print("Couldn't find last backup date")
    count = 0
    requiredlogs = []
    #print("lOGS IN mISSIONPLANNER:",MissionplannerLogs)
    exceptions = False
    for log in MissionplannerLogs:
        logdate = (log.split(" ", 1)[0])
        try:
            if logdate not in LOGsorteddates and datetime.datetime.strptime(logdate, '%Y-%m-%d'):
                if logdate not in requiredlogs:
                    count = count+1
                    requiredlogs.append(logdate)

        except:
            print(colour('red'),"Cannot find date in file-name:",log)
            exceptions = True

    if exceptions:
        if input(colour('yellow')+"File name errors found. Do you wish to exclude them and continue (y/n)?: "+colour('white')) == 'y':
            print("Total Logs found after last backup:",count)
            if count > 0:
                print("RequiredLogs:",requiredlogs)
                backup(requiredlogs)
            else:
                print("You are upto-date!")

        else:
            print("Rectify error manually and Please try again!")
            open_file(MissionplannerLogLocation)
            print("Opening file path!")
            exit()
    else:
        print("Total Logs found after last backup:",count)
        if count > 0:
            print("RequiredLogs:",requiredlogs)
            requiredlogs.sort()
            backup(requiredlogs)
        else:
            print("You are upto-date!")


if __name__ == "__main__":
    print("LOGIT!")
    try:
        CurrentStatus()
    except KeyboardInterrupt:
        print("Exiting Logit!")
