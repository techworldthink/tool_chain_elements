#     An Image indexing script part of tool_chain_elements for GIS application
#     Copyright (C) 2018  ICFOSS
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#     Author: Vaisakh Anand (gitlab: @vaisakh032)
# /*******************************************************************************
#  * index.py
#  *******************
#     This script will help you index the images that were taken in flight to those
#     details that you are enter. It will help manage processed data and help make
#     reports based on it.
#  *********************************************************************************

import os
import sys
from PIL import Image
from PIL import ExifTags
import readline
import json
import time
from TabAutoCompletion.tabCompletion import Tabcomplete
from datetime import datetime


class colors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    GREEN = '\033[92m'
    WARNING = '\033[41m'
    FAIL = '\033[91m'
    BOLD = '\033[1m'
    ITALICS = '\033[3m'
    UNDERLINE = '\033[4m'
    BGYellow = '\033[43m'
    END = '\033[0m'
    BLINK = '\033[5m'
    GREY = '\033[90m'


imagesetregster = os.path.expanduser("~")+"/.DroneImageSetRegister_droneToolChainn.json"
tabCompletionFile = os.path.expanduser("~")+"/.TabCompletionFile_droneToolChain.json"
registrationNumber = 0
data = []

print("\n\n\n", colors.BGYellow, "Image Index Stored at: ", colors.END, colors.GREY, imagesetregster, colors.END, "\n\n\n\n")
time.sleep(3)

# dict = [
#             "ID": ID,
#             "area": Area,
#             "equipment": equipment,
#             "description": description,
#             "date": date,
#             "geotagged": geotagged,
#             "camAngle" : angle,
#             "flightType": flightType,
#             "altitude" : altitude
#             "images": numberOfImages
#             "directory": location of images
#     ]



def get_registration_number():
    """The function will check for the number of registrations in the specified json file.

    If json file is found, then the total number of registrations
    i.e indexes are calculated to initialise the global varialble registrationNumber.

    Paramaters: none
    Returns: none
    """
    global registrationNumber
    global imagesetregster
    global data
    if os.path.exists(imagesetregster):
        with open(imagesetregster) as f:
            data = json.load(f)
        registrationNumber = data[len(data)-1]["ID"] + 1
    else:
        # create tabcomplete file
        tempdata = {"equipment": [], "area": [], "flightType": []}
        with open(tabCompletionFile, 'w') as outfile:
            json.dump(tempdata, outfile)
        registrationNumber = 1

    print("Registration till now:", registrationNumber-1)


    # exif = {
    # ExifTags.TAGS[k]: v
    # for k, v in data.items()
    # if k in ExifTags.TAGS
    # }
    # print(path)
    # print("raw Data: ", data)
    # print("Exif : ",exif)
    # print("GPS Alt: ",)
    # exit()

def get_gps_data(path):
    """The function is intended to extract required exif data from the images
    that are required for indexing the images.
    Parameters:
     -> Path: Path to the image file from which data has to be extracted.
     Return:
      The function returns a list that will contain the information in the
      following format.
        [gps altitude, geotaged (0 or 1), date and time of image captured.]
    """
    data = Image.open(path)._getexif()
    geoTagged = 0
    if data != {}:
        try:
            if data[34853] != {}:
                geoTagged = 1

            data = [data[34853][6][0]/data[34853][6][1],geoTagged,str(data[36867])]
            print(data)
            return data
        except:
            print("No Exif Data for image: ",colors.WARNING,str(path).split('/')[-1],colors.END)
            return [0,0,0]

def check_for_variation_in_alt(altitudeList):
    """Funtion to check for variation in altitude tags in exif data.
    Parameters:
     -> altitudeList : a list containing altitude extracted from all the images
                       in the imageset.
    Return:
     Returns true if the variation in the image altitudes is less than 10m or if
     variation approved by the user.
    """
    if max(altitudeList) - min(altitudeList) > 10:
        while True:
            print(colors.BGYellow,colors.ITALICS,"Variation in altitude tags of images is more than 10 m", colors.END)
            ip = input("\a\t\tShall i continue (y/n): ").lower()
            if ip == 'y':
                break
            elif ip == 'n':
                exit(0)
    return True

def register_imageSet(path):
    """
    The function is intended to index the image sets in the given path.

    The functions get's the list of images in the directory (path) given
    and cross checks if the image was indexed before. If none of the images
    in the directory was added before , then details required for indexing
    the images are collected from the user using an other function called
    get_details(). the images in the register is appeneded onto a list which
    is then linked with the index details.
    The function also invokes updateJson() to update the newly added detail
    on the json file
    Parameters:
    path : path to the location containing the imagesetregster.
    Returns: none
    """
    global registrationNumber
    print("Registering Images")
    directory = os.path.abspath(os.getcwd()+"/"+path)
    files = os.listdir(directory)  # get current working Directory\
    imageset = []
    gps_info = {"alt":[], "geoTagged":[], "date":[]}
    for images in files:
        if images.endswith(".jpg"):
            extractedData = get_gps_data(directory+'/'+images)
            gps_info["alt"].append(extractedData[0])
            gps_info["geoTagged"].append(extractedData[1])
            if extractedData[2].split(" ")[0] not in gps_info["date"] and type(extractedData[2]) == type(''):
                gps_info["date"].append(extractedData[2].split(" ")[0])
            images = str(extractedData[2])+"|"+images
            if registrationNumber == 1:
                imageset.append(images)
            else:
                if not check_for_registration(images):  # check if the image was registered before in any other imageset
                    imageset.append(images)
                else:
                    print("Conflict in image regitration.  -> "+images+" have been registered before!")
                    return 0
    print("Total Number of images Found : ", len(imageset))
    if len(imageset) <= 0:
        print("No images were found to register")
        return 0

    passList = [] # arguments to be passed to get_details() function
    if check_for_variation_in_alt(gps_info["alt"]):
        passList.append(round(sum(gps_info["alt"])/(len(gps_info["alt"]))))

    if int(sum(gps_info["geoTagged"])/(len(gps_info["geoTagged"]))) == 1:
        passList.append('y')
    else:
        passList.append('n')
    if len(gps_info["date"]) == 1:
        gps_info["date"] = datetime.strptime(gps_info["date"][0], '%Y:%m:%d').strftime('%d.%m.%Y')
        passList.append(gps_info["date"])

    else:
        passList.append("0")

    details = get_details(passList[0], passList[1], passList[2])   # get imageset description
    details["images"] = imageset
    details["directory"] = directory
    register_name_set = False
    while register_name_set == False:   # for confirmation of entered data
        display_report([details])
        while True:
            ip = input("Are you sure to enlist all the images under the above description (y/n):").lower()
            if ip == 'y':
                register_name_set = True
                break
            elif ip == 'n':
                details = get_details(passList[0], passList[1], passList[2])      # get imageset description
                details["images"] = imageset
                details["directory"] = directory
                break
    updateJson(details)
    registrationNumber = registrationNumber + 1


def check_for_registration(image):
    """
    Function to check if the image name passed as the Parameter have been indexed before.
    Paramaters:
    image : name of an image file with its date and time taken as prefix.
    Return:
     boolean function that returns True if the image name passes has already been
     registered before.
    """
    global data
    for details in data:
        if image in details["images"]:
            return 1
    return 0


def get_image_registration(image):
    """
    Function to fetch and return the registration details of an indexed image.

    The function runs through the data dictionary checks of registration details
    of the images in every indexes. If index is found, it return registration
    details as a dictionary.
    Parameter:
    image: image name with date and time as prefix sepereated by "|"
    Returns:
    Return the index details if image found in any of the indexes and returns 0
    if not details found.
    """
    global data
    for details in data:
        if image in details["images"]:
            return details
    print("Image not found in register: ", colors.FAIL, image, colors.END)
    return 0



# Get details from the user to index the images
def get_details(altitude = 0, gtag = "0", date = "0"):
    """
    Function to fetch details from the user for indexing the image set.

    The function gets details from user and return a dictionary so that it can
    be easily worked. The function also imparts auto tab complete feature so that
    the user has to just press tab to show all the possible values. Documentation
    of details regarding the TabComplete class can be found at:
    https://gitlab.com/vaisakh032/tab-auto-completion
    """
    global registrationNumber
    print("Tab completion at: ", tabCompletionFile)
    completer = Tabcomplete(tabCompletionFile)
    details_set = False
    details = {}
    print("\n\n..**Enter Image Set Description**..")
    while details_set is False:
        details["ID"] = registrationNumber
        details["area"] = completer.getip("Area>> ", "area")   # input("Area: ")
        details["equipment"] = completer.getip("Equipment>> ", "equipment")   # input("Equipment: ")
        details["description"] = input("Enter Description (make it short): ")
        if date == "0":
            details["date"] = input("Enter date of image capture: ")
        else:
            print("Date extracted from images is ",colors.BGYellow,date,colors.END,colors.GREY,colors.ITALICS,)
            details["date"] = str(input("if no objection hit enter or else enter the correct date: "+colors.END) or date)
        geotagged = ''
        if gtag == "0":
            geotagged = input("GeoTagged (y/n): ")
        else:
            geotagged = str(gtag)
            print("Images in Set is geoTagged (y/n): ",gtag)
        if (geotagged == 'y' or geotagged == 'Y'):
            details["geotagged"] = "GTagged"
        else:
            details["geotagged"] = "Untagged"
        details["angle"] = input("Camera Angle: ")
        details["flightType"] = completer.getip("Type Of Survey >> ","flightType") #input("Enter type of Sruvey: ")
        if altitude == 0:
            details["alt"] = input("Enter Altitude of Flight:")
        else:
            details["alt"] = altitude
            print("Altitude Of Flight Extracted From Images: ",details["alt"])
            time.sleep(1)
        return details

# Update the json file that keeps the index details
def updateJson(dict=[]):
    """Function to update the json file containing the index of images.

    Parameters:
     -> dict = dictinary of image indexes to be updated in the json file
    """
    global data
    if dict != []:
        data.append(dict)
    with open(imagesetregster, 'w+') as outfile:
        json.dump(data, outfile)


def get_report(path):
    """Function to fetch the details of images in the given path.

    The function walks through all the images in the directory and fetches
    exif details of those images using the function get_gps_data(path). Each
    time a new image is scanned,the details fetched are appended and if was
    found before, it will append the total number of scanned images in the folder.

    Parameters:
     -> path : the path to the folder containing the imageset

    """
    jpgfiles = os.listdir(os.getcwd()+'/'+path)
    reportOutput = []
    numberOfImages = 0
    for images in jpgfiles:
        if images.endswith('.jpg'):
            numberOfImages = numberOfImages + 1
            images = str(get_gps_data(os.getcwd()+'/'+path+'/'+images)[2])+"|"+images
            output = get_image_registration(images)
            if output not in reportOutput and output != 0:
                output["scanned"] = 1
                reportOutput.append(output)
            elif output in reportOutput and output != 0:
                for k, details in enumerate(reportOutput):
                    if details["ID"] == output["ID"]:
                        reportOutput[k]["scanned"] = reportOutput[k]["scanned"] + 1

    if reportOutput != 0:
        print("Total Number of images: ", numberOfImages, " Number of images indexed:", sum(img['scanned'] for img in reportOutput))
        display_report(reportOutput)

def editRegister_byID():
    """Funtion to get index ID from user and edit its detail.
    """
    global data
    index = input("Enter ID of Imageset: ")
    hit_flag = -1
    for i, details in enumerate(data):
        if str(details["ID"]) == index:
            hit_flag = True
            display_report([details])
            print("index = ", i)
            new_details = editDetails(details)
            if new_details != []:
                display_report([new_details])
                data[i] = new_details
                print(data)
                print("List Edited")
            else:
                print ("Error happened")

def editDetails(old_details):
    """Function to fetch details from the user so as to edit a previously
    indexed imageset.
    Parameters:
     -> old_details : The dictionary containing old index details

     Return:
      Return new details that are entered by the user
    """
    new_details = {}
    completer = Tabcomplete(tabCompletionFile)
    while True:
        new_details = get_details()
        new_details["ID"] = old_details["ID"]
        new_details["images"] = old_details["images"]
        new_details["directory"] = old_details["directory"]
        display_report([new_details])
        if input("Do you wish save the new changes (y/n)").lower() == 'y':
            break
    return new_details

# display_report shoudld get paramater as list
def display_report(details):
    if len(details) < 1:
        print(colors.WARNING, "No images in the Directory matched with register. Please cross check", colors.END)
        return 0;
    print("\n\n\nNumber of Distinct Flights in directory: "+colors.BLINK, len(details), colors.END)
    for flight in details:
        if flight == []:
            return 0
        print("****************************************************************")
        print(colors.BGYellow+"Flight ID:"+colors.END, flight["ID"], "\t\t", flight["directory"])
        if 'scanned' in flight:
            print("Images Register in Set:", len(flight["images"]), "\t\tImages Found:", flight["scanned"])
        else:
            print("Images Register in Set:", len(flight["images"]))
        print("****************************************************************")
        print("Equipment: ", flight["equipment"])
        print("Area:", flight["area"], "\t\tDate:", flight["date"])
        print("Type:", flight["flightType"], "\t\t\tAlt: ", flight["alt"], "m")
        if flight["geotagged"] == "GTagged":
            print("Cam :", flight["angle"], "deg \t\t\t\tGeo:", colors.GREEN, flight["geotagged"], colors.END)
        else:
            print("Cam :", flight["angle"], "deg \t\t\t\tGeo:", colors.WARNING, flight["geotagged"], colors.END)
        print("Description:\n", colors.GREY, colors.ITALICS, flight["description"], colors.END)


if __name__ == "__main__":
    get_registration_number()
    if len(sys.argv) < 2:
        print("No Arguments found. Please Refer --Help")
        exit(0)
    else:
        if(sys.argv[1]!= "-e"):
            directory = sys.argv[2:]
            directory = os.path.abspath(os.getcwd()+"/"+sys.argv[2])
            if not os.path.exists(directory):
                print("Directory: ", colors.FAIL, directory, " does not exist.", colors.END)
                exit(0)
        if(sys.argv[1] == "-s"):
            print("Searching for registered images in given directory")
            get_report(sys.argv[2])
        elif (sys.argv[1] == "-r"):
            print("Registring")
            register_imageSet(sys.argv[2])
        elif (sys.argv[1] == "-e"):
            print("Edting the Register")
            ip = input("Search by\n\t-d Date and Equipment \n\t-i ID \n\t-a Area and Date\n\t  :")
            if ip == 'd':
                date = str(input("Enter date: "))
                equipment = input("Enter Equipment: ")
                for i, details in enumerate(data):
                    if date == details['date']:
                        if equipment.lower() in details['equipment'].lower():
                        #if re.match(equipment.lower(),details['equipment'].lower()):
                            display_report([details])
                            input("\nPress Enter to continue....")

            if ip == 'i':
                editRegister_byID()
            if ip == 'a':
                area = input("Enter the area: ")
        else:
            print("Unknow Parameter. Please Refer --Help")
