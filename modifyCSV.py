 #     A cript to modify csv files part of tool_chain_elements for GIS application
 #     Copyright (C) 2018  ICFOSS
 #
 #     This program is free software: you can redistribute it and/or modify
 #     it under the terms of the GNU General Public License as published by
 #     the Free Software Foundation, either version 3 of the License, or
 #     (at your option) any later version.
 #
 #     This program is distributed in the hope that it will be useful,
 #     but WITHOUT ANY WARRANTY; without even the implied warranty of
 #     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 #     GNU General Public License for more details.
 #
 #     You should have received a copy of the GNU General Public License
 #     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 #
 #     Author: Vaisakh Anand (gitlab: @vaisakh032)
 # /*******************************************************************************
 #  * modifyCSV.py
 #  *******************
 #   This script helps you to midify the location.csv produced by the MissionPlanner
 #   so that it could directly loaded on the exiftool.
 #  *********************************************************************************
import os

csv = open("location.csv").readlines()
print("Files Data:")

files = open("modifiedlog.csv", "w")
files.write("SourceFile,GPSLatitude,GPSLongitude,GPSAltitude\n"+str(os.getcwd())+"/")
for lines in csv:
    print(lines)
    files.write(str(lines))
