#     automate.sh to ease the use of python scripts in this tool chain
#     Copyright (C) 2018  ICFOSS
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#
# /*******************************************************************************
#  * automate.sh
#  *******************
#   This shell script in intended to auto add shell command to .basrc and help the
#   user to run the scripts from the terminal to invoke these python scripts.
#  *********************************************************************************



echo Installing required libraries

tput setaf 1 #set colour to red
read -p 'Install pip 2 (y/n)? ' response
tput sgr0    #reset colour
response=${response,,} # tolower
if [[ $response =~ ^(yes|y| ) ]] || [[ -z $response ]];
then
  sudo apt-get install python-pip -y
fi

tput setaf 1 #set colour to red
read -p 'Install pip 3 (y/n)? ' response
tput sgr0    #reset colour
response=${response,,} # tolower
if [[ $response =~ ^(yes|y| ) ]] || [[ -z $response ]];
then
  sudo apt-get install python3-pip -y
fi


tput setaf 1 #set colour to red
read -p 'Install required libraries(y/n)?: ' response
tput sgr0    #reset colour
response=${response,,} # tolower
if [[ $response =~ ^(yes|y| ) ]] || [[ -z $response ]];
then
  sudo pip3 install -r requirementsPython3.txt
  sudo pip2 install -r requirementsPython2.txt
fi

tput setaf 1 #set colour to red
read -p 'Install exiftool (used for geotagging images) (y/n)? ' response
tput sgr0    #reset colour
response=${response,,} # tolower
if [[ $response =~ ^(yes|y| ) ]] || [[ -z $response ]];
then
  sudo apt-get install -y libimage-exiftool-perl
fi

tput setaf 1 #set colour to red
read -p 'Clone Tab Auto-Complete python module for running index.py (y/n)?: ' response
tput sgr0    #reset colour
response=${response,,} # tolower
if [[ $response =~ ^(yes|y| ) ]] || [[ -z $response ]];
then
  echo "Cloning Tab Auto complete python project into directoy"
  git clone https://gitlab.com/vaisakh032/tab-auto-completion.git
  echo "Renaming 'tab-auto-completion' to 'TabAutoCompletion'"
  mv tab-auto-completion TabAutoCompletion
fi

if [ -e .droneMappingRunCommands.sh ] #to check if the shell script in already created and added to bashrc
then
  echo
  echo
  tput setaf 3 #set colour to yellow
  echo "droneMappingRunCommands.sh shell script has already been added into .bashrc"
  tput dim     # dim the output
  tput setaf 7 #set colour to white
  echo "Please run ->> nano ~/.bashrc and check for: "
  tput setaf 2 #set colour to green
  echo "    #Adding shell scripts for Drone_Tool_Chain"
  echo
  tput sgr0    #reset colour
  tput setaf 3 #set colour to yellow
  echo "And if not linked in bash.rc please run: "
  tput dim     # dim the output
  tput setaf 7 #set colour to white
  echo "    rm .droneMappingRunCommands.sh and re run automate.sh"
  tput sgr0    #reset colour
  exit 0
fi

#to create a .droneMappingRunCommands.sh which would act as source for terminal commands
tput setaf 3 #set colour to yellow
echo "Creating .droneMappingRunCommands.sh in directory $PWD"
tput sgr0    #reset colour
touch $PWD/.droneMappingRunCommands.sh
printf '%s\n' '#!/bin/sh' '#Commands to run logit.py' 'function logit(){' \
    'echo "Opening Logit"' \
    'sudo python3 '"$PWD"'/logit.py' \
    '}' '''#Commands to run tagit.py' \
    'function tagit(){' \
    'echo "Modifying CSV file"' 'python3 '"$PWD"'/modifyCSV.py' \
    'echo "Running tagit.py"' 'python '"$PWD"'/tagit.py' \
    'echo "Running EXIFTool"' 'exiftool -csv="modifiedlog.csv" "."' \
    'rm *.jpg_origi*' '}' '''#To find untagged images in the directory' \
    'function finduntagged(){' \
    'echo "Scanning images in directory which are not GeoTagged...please wait patiently"' \
    'for file in *.jpg; do out=$(exiftool $file | grep 'GPS Position') ; if [ "$out" = "" ]; then echo $file; fi; done' \
    'echo "Scan Complete!"' '}' '' '#Commands to run backup.py' \
    'function backup(){' \
    'if [ "$#" -eq  "0" ]' ' then' '   echo "No arguments supplied"' '   echo "Usage: backup /give/destination/directory/hre "' \
    'else' '  echo "running backup.py" '  '  python3 ~/FlightLogs/backup.py $1' 'fi' '}' '' '#Commands to rename JPGs to jpg in the directory' \
    'function renamejpg() {' \
    'echo "Renaming *.JPG to *.jpg "' 'rename "s/\.JPG$/\.jpg/" *.JPG' '}' '' '#Commands to run index.py to index images'\
    'function indeximages() {'\
    'echo "Starting index.py"' 'python3 '"$PWD"'/index.py -r "$1"' '}' '' '#Commands to run index.py to search images indexes'\
    'function getinfo() {'\
    'echo "Starting index.py"' 'python3 '"$PWD"'/index.py -s "$1"' '}' '' '#Commands to run index.py to edit images indexes'\
    'function editindex() {'\
    'echo "Starting index.py"' 'python3 '"$PWD"'/index.py -e' \
    '}' > $PWD/.droneMappingRunCommands.sh

#to add the the new shell script file into .bashrc as a source file
tput setaf 1 #set colour to red
read -p 'Should i add .droneMappingRunCommands.sh to .bashrc (y/n)?: ' response
tput sgr0    #reset colour
response=${response,,} # tolower
if [[ $response =~ ^(yes|y| ) ]] || [[ -z $response ]];
then
  echo "Linking droneMappingRunCommands.sh shell script file into .bashrc"
  echo "#Adding shell scripts for Drone_Tool_Chain"  >> ~/.bashrc
  echo "source $PWD/.droneMappingRunCommands.sh" >> ~/.bashrc
fi
