#     A script to GeoTag images as a  part of tool_chain_elements for GIS application
#     Copyright (C) 2018  ICFOSS
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#      Author: Vaisakh Anand (gitlab: @vaisakh032)
# /*******************************************************************************
#  * tagit.py
#  *******************
#   This program will help you GeoTag images from the location.txt file created
#   by MissionPlanner. This textfile provides the lat long cordinates and altitude
#   to be tagged into the images.
#  *********************************************************************************

from pexif import JpegFile
import os
import shutil
overwrite = True


def insert_into_image(image_name, lat, lon):
    global overwrite
    if overwrite:
        destination = image_name
    else:
        destination = "geotag/"+image_name

    if not os.path.exists(image_name):
        print("Image not Found. Image Name:", image_name)
        return 0
    else:
        if not overwrite:
            shutil.copy(image_name, destination)
    output = JpegFile.fromFile(destination)
    output.set_geo(lat, lon)
    output.writeFile(destination)


if not overwrite:
    if os.path.exists("geotag"):
        pass
    else:
        os.mkdir("geotag")
        print("Creating Folder: geotag, yor geotagged images will be saved there")

if not os.path.isfile("location.txt"):
    print("Location.txt not found")
    exit()

file = open("location.txt").readlines()
for lines in file:
    if lines.startswith("#"):
        continue
    else:
        lines = lines.split(" ")
    print("Image Name:", lines[0])
    insert_into_image(lines[0], float(lines[1]), float(lines[2]))
