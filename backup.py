#     A backup script part of tool_chain_elements for GIS application
#     Copyright (C) 2018  ICFOSS
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#      Author: Vaisakh Anand (gitlab: @vaisakh032)
# /*******************************************************************************
#  * backup.py
#  *******************
#   This program will help you to backup you source folder to a destination folder
#   by copying only the new files updated that are unsynced between each other. This
#   is intended for backing up your flight logs and data collected from flight in
#   particular interval so that, your backup will contain files that you deleted from
#   your source folder backups away.
#   _______________________________________________________________________________
#   Note:
#   This script will only add newly updated files into
#   your backup folder and will not delete any that was deleted from your source.
#   ________________________________________________________________________________
#  *********************************************************************************


import os
import shutil
import sys
import stat

source_folder = os.getcwd()
source_folder = source_folder.rstrip('/')
print("Source Folder: ", source_folder)
if not len(sys.argv) > 1:
    print("Please enter destination Folder as a parameter")
    exit()
else:
    destion_folder = sys.argv[1]
if destion_folder.endswith('/'):
    destion_folder.rstrip('/')

if not os.path.exists(destion_folder):
    print("please enter a valid destination folder as arguments!")
    exit()
else:
    print("Destination Folder:", destion_folder)
ignorelist = [[] , []]
final_list_source = []
final_list_destination = []
copy_files_list = []

if os.path.exists(source_folder+"/.backupignore.txt"):
    ignore_files = open(source_folder+"/.backupignore.txt").readlines()
    for lines in ignore_files:
        if lines.startswith("#ignore Folders with name"):
            headcount = 0
            continue
        if lines.startswith("#ignore Files with extension"):
            headcount = 1
            continue
        if lines.rstrip() != '':
            ignorelist[headcount].append(lines.rstrip())
else:
    print(".backupignore.txt file created in the source folder.\nPlease add details into the .backupignore.txt and restart the program.")
    temp = open(source_folder+"/.backupignore.txt", "w+")
    temp.write("#ignore Folders with name\n\n#ignore Files with extension\n")
    temp.close()
    exit()


def subdirSkip(subdir):
    global ignorelist
    for ele in subdir.split('/'):
        if ele in ignorelist[0]:
            return 0
    return 1


def fileSkip(filename):
    for ele in ignorelist[1]:
        if filename.endswith(ele):
            return 0
    return 1


def get_final_list_of_files(directory):
    global ignorelist
    global ignore_files
    final_list = []
    for subdir, dirs, files in os.walk(directory):
        if subdirSkip(subdir):
            subdir = subdir.replace(directory, '')
            for file in files:
                if fileSkip(file):
                    if subdir.startswith('/'):
                        final_list.append(subdir+'/'+file)
                    else:
                        final_list.append('/'+subdir+'/'+file)
        else:
            continue
    return final_list


def copytree(src, dest):
    directory = os.path.dirname(dest)
    if os.path.exists(os.path.dirname(dest)):
        print("Folder exists")
    else:
        print("Creating Destination Folder:")
        retry = True
        directory = dest
        while retry:
            directory = os.path.dirname(directory)
            print(directory)
            try:
                os.mkdir(directory)
                print("Creating Directory Succesful")
                retry = False
            except:
                retry = True
                print("Retrying to make directory:")
            if not os.path.exists(os.path.dirname(dest)) and not retry:
                print("Folder Tree Creation not successfull")
                retry = True
                directory = dest

    shutil.copy(src, dest)


def scan_and_confirm():
    global final_list_source
    global final_list_destination
    global copy_files_list
    print("Scanning Files")
    for file_s in final_list_source:
        hit = False
        for file_d in final_list_destination:
            if str(file_s) == str(file_d):
                hit = True
        if not hit:
            copy_files_list.append(file_s)

    print("Number of files to be updated: ", len(copy_files_list))
    if len(copy_files_list) > 0:
        print("Required Files to be copied:")
        for ele in copy_files_list:
            print(ele)
    else:
        print("Files Backup uptodate")



if __name__ ==  "__main__":
    print("Ignore Folders with Name:", ' '.join(str(ele) for ele in ignorelist[0]))
    print("Ignore Files with Extensions:", ' '.join(str(ele) for ele in ignorelist[1]))
    print("Scanning Files in Source Directory")
    final_list_source = get_final_list_of_files(source_folder)
    print("Total Number of files in Source Folder: ", len(final_list_source))
    print("Scanning Files in Destination Directory")
    final_list_destination = get_final_list_of_files(destion_folder)
    print("Total Number of files in Destination Folder: ", len(final_list_destination))
    scan_and_confirm()
    for ele in copy_files_list:
        print("copy from:", source_folder+ele, " to ", destion_folder+ele)
        copytree(source_folder+ele, destion_folder+ele)
    print ("\n\nCopying Completed\n", destion_folder, "is up to date")
